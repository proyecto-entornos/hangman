/**
* Write a description of class Ahorcado here.
* 
* @author Álvaro Serrano Lozano & Juan Diego Fernandez Arroyavez
* @version 25/02/2019
*/
import java.util.Scanner;
public class Ahorcado
{
    public int vidas;                                               //vidas o intentos que tiene el jugador para descubrir la palabra
    public String palabraFinal;                                     //Variable que comprobara las letras ingresadas y la palabra en la array
    public Scanner entrada;                                         //Escanea las letras que metemos en la terminal 
    public String palabraJuego;                                     //Variable que escoge la palabra de la array
    public String[] letrasPalabras;                                 //Array donde guardara las letras
    public String palabraOculta;
    public String[] palabraresp;
    public String[] letraIngresada;
    
    public Ahorcado () {                                                           //Constructora
                }        
    public void EligePalabra() {
           String palabras[]={"hola","chao","java","corazon","automovil"};         //palabras del juego
           palabraFinal = "";                                                      //comprueba la palabra formada con las letras ingresas y la variable palabraJuego
           entrada=new Scanner(System.in);                                         //entrada de letras desde teclado
           palabraJuego= palabras[(int) (Math.random()*palabras.length)];          //palabra al azar sacada del array palabras
           letrasPalabras =new String[palabraJuego.length()];
           palabraOculta="";                                                      //variable Que almacenara los "_" de la palabra
    }
    public void CambioPalabra() {                                                         //for para separar palabra por letras   
           palabraresp=new String[palabraJuego.length()];                            //transformando la palabra dependiendo del largo a la cantidad de "_" correspondientes
           for(int i=0;i<palabraJuego.length();i++){
                letrasPalabras[i]=String.valueOf(palabraJuego.charAt(i));
                palabraOculta=palabraOculta+" _ ";
                palabraresp[i]=" _ ";
           }
    }
    public void InicioJuego() {
           System.out.println("Suerte con tus intentos recuerda que tienes: "+vidas+ " vidas");
           System.out.println();
           System.out.println(palabraOculta);                                      //muestro la los "_" correspondientes a la palabra ocult
           letraIngresada=new String[100];                                         //letras ingredas por el jugador        
    }    
    public void InicioLetra(int q) {
           System.out.println("Ingrese una letra: ");
           letraIngresada[q]=entrada.nextLine().toLowerCase();
           if(q>=1){                                           //Este buble evita que el jugador vuelva a poner una letra ya puesta,
              for(int a=0;a<q;a++){                               //por lo tanto no le resta vida y le sale un mensaje de error.
                  if(letraIngresada[a].equalsIgnoreCase(letraIngresada[q])){
                     System.err.println("Ya Ingresaste esa letra, Intenta denuevo...");
                  }
              }
           }
    } 
    public void FuncionJuego(int q) {
               if(palabraJuego.contains(letraIngresada[q])==true){                                 //si crees que sabes la palabra puedes ponerla y 
                    if(palabraJuego.equalsIgnoreCase(letraIngresada[q])){                           //dependiendo de lo que pongas ganaras o perderas
                        System.out.println("GANASTE, La palabra oculta era: "+palabraJuego);
                        System.exit(0);
                    }
                    for(int e=0;e<palabraJuego.length();e++){                   //Este bucle recorre la palabra del juego y compara si la letra ingresada coincide con la palabra
                        if(letraIngresada[q].equalsIgnoreCase(letrasPalabras[e])){              // si es asi se reemplaza el "_" por  la letra introducida
                            palabraresp[e]=letraIngresada[q];
        
                        }
                    }
                    //guardo cada posicion del array de letras correctas y las meto solo en una variable tipo String
                    String palabraW="";
                    for(int z=0;z<palabraJuego.length();z++){
                        palabraW=palabraW+palabraresp[z];
        
                    }
        
                    palabraFinal=palabraW;                  //La variable se actualiza cada vez que se ingresa una letra, 
                    //para ir comparando si es = a la palabra original
                    System.out.println(palabraW);           //muestro al jugador las letras que lleva acertadas
                    System.out.println();
        
               }
               else{
                    System.out.println("no esta, una vida menos "+ vidas);  //Si fallas Te sale un error y se resta una vida
                    vidas--;
                    String palabraL="";
                    for(int z=0;z<palabraJuego.length();z++){
                        palabraL=palabraL+palabraresp[z];
                    }
        
                    System.out.println(palabraL);                     //muestra las letras descubiertas en la palabraOculta hasta el momento
                    System.out.println();
                    if(vidas==-1){                                    //si se acaban las vidas termina el juego
                        System.out.println("GAME OVER! la palabra oculta era " +palabraJuego);
                    }
               }
               if(palabraFinal.equalsIgnoreCase(palabraJuego)){      //si la palabraFinal es llenada correctamente letra por letra el jugador gana
        
                    System.out.println("GANASTE, La palabra oculta era: "+palabraJuego);
                    System.exit(0);
               }
    }
    public static void main(String[] args) {
        Ahorcado ahorcado = new Ahorcado();
        ahorcado.vidas=6;                                                        //vidas o intentos que tiene el jugador para descubrir la palabra
        ahorcado.EligePalabra(); 
        ahorcado.CambioPalabra();
        ahorcado.InicioJuego();                                                 //inicio del juego
        
        // inicio del ingreso de letras llenando el array de letras ingresadas por el jugador
        for(int q=0;q<ahorcado.letraIngresada.length;q++){
            ahorcado.InicioLetra(q);
            ahorcado.FuncionJuego(q);
            if(ahorcado.vidas==5){
                System.out.println("   _______________________  ");
                System.out.println("  ||                      ¦ ");
                System.out.println("  ||                      ¦ ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("  ||                        ");
                System.out.println("[____]                      ");
            }
            if(ahorcado.vidas==4){
                System.out.println("   _______________________      ");
                System.out.println("  ||                      ¦     ");
                System.out.println("  ||                      ¦     ");
                System.out.println("  ||                   * * * *  ");
                System.out.println("  ||                  *  >  < * ");
                System.out.println("  ||                  *   __  * ");
                System.out.println("  ||                   #######  ");
                System.out.println("  ||                            ");
                System.out.println("  ||                            ");
                System.out.println("  ||                            ");
                System.out.println("  ||                            ");
                System.out.println("  ||                            ");
                System.out.println("  ||                            ");
                System.out.println("  ||                            ");
                System.out.println("  ||                            ");
                System.out.println("  ||                            ");
                System.out.println("  ||                            ");
                System.out.println("  ||                            ");
                System.out.println("  ||                            ");
                System.out.println("  ||                            ");
                System.out.println("  ||                            ");
                System.out.println("  ||                            ");
                System.out.println("[____]                          ");
            }
            if(ahorcado.vidas==3){
                System.out.println("   _______________________        ");
                System.out.println("  ||                      ¦       ");
                System.out.println("  ||                      ¦       ");
                System.out.println("  ||                   * * * *    ");
                System.out.println("  ||                  *  >  < *   ");
                System.out.println("  ||                  *   __  *   ");
                System.out.println("  ||                   #######    ");
                System.out.println("  ||                 #         #  ");
                System.out.println("  ||                  #  0 1  #   ");
                System.out.println("  ||                  #  1 0  #   ");
                System.out.println("  ||                  #  0 1  #   ");
                System.out.println("  ||                  #  1 0  #   ");
                System.out.println("  ||                  #       #   ");
                System.out.println("  ||                   #######    ");
                System.out.println("  ||                              ");
                System.out.println("  ||                              ");
                System.out.println("  ||                              ");
                System.out.println("  ||                              ");
                System.out.println("  ||                              ");
                System.out.println("  ||                              ");
                System.out.println("  ||                              ");
                System.out.println("  ||                              ");
                System.out.println("[____]                            ");
            }
            if(ahorcado.vidas==2){
                System.out.println("   _______________________              ");
                System.out.println("  ||                      ¦             ");
                System.out.println("  ||                      ¦             ");
                System.out.println("  ||                   * * * *          ");
                System.out.println("  ||                  *  >  < *         ");
                System.out.println("  ||                  *   __  *         ");
                System.out.println("  ||                   #######          ");
                System.out.println("  ||                /#         #\\      ");
                System.out.println("  ||               /  #  0 1  #  \\     ");
                System.out.println("  ||              /  /#  1 0  #\\  \\   ");
                System.out.println("  ||             /  / #  0 1  # \\  \\  ");
                System.out.println("  ||            /  /  #  1 0  #  \\  \\ ");
                System.out.println("  ||            ***   #       #   ***   ");
                System.out.println("  ||                   #######          ");
                System.out.println("  ||                                    ");
                System.out.println("  ||                                    ");
                System.out.println("  ||                                    ");
                System.out.println("  ||                                    ");
                System.out.println("  ||                                    ");
                System.out.println("  ||                                    ");
                System.out.println("  ||                                    ");
                System.out.println("  ||                                    ");
                System.out.println("[____]                                  ");
            }
            if(ahorcado.vidas==1){
                System.out.println("   _______________________                ");
                System.out.println("  ||                      ¦               ");
                System.out.println("  ||                      ¦               ");
                System.out.println("  ||                   * * * *            ");
                System.out.println("  ||                  *  >  < *           ");
                System.out.println("  ||                  *   __  *           ");
                System.out.println("  ||                   #######            ");
                System.out.println("  ||                /#         #\\        ");
                System.out.println("  ||               /  #  0 1  #  \\       ");
                System.out.println("  ||              /  /#  1 0  #\\  \\     ");
                System.out.println("  ||             /  / #  0 1  # \\  \\    ");
                System.out.println("  ||            /  /  #  1 0  #  \\  \\   ");
                System.out.println("  ||            ***   #       #   ***     ");
                System.out.println("  ||                   #######            ");
                System.out.println("  ||                  /   ¦   \\          ");
                System.out.println("  ||                  |   ¦   |           ");
                System.out.println("  ||                  |   ¦   |           ");
                System.out.println("  ||                 _/   ¦   \\_         ");
                System.out.println("  ||                /_____|_____\\        ");
                System.out.println("  ||                                      ");
                System.out.println("  ||                                      ");
                System.out.println("  ||                                      ");
                System.out.println("[____]                                    ");
            }
            if(ahorcado.vidas==0){
                System.out.println("   _______________________       ______________   ");
                System.out.println("  ||                      ¦     |              |  ");
                System.out.println("  ||                      ¦     | ULTIMA VIDA! |  ");
                System.out.println("  ||                   * * * *  |  ____________|  ");
                System.out.println("  ||                  *  >  < *  V                ");
                System.out.println("  ||                  *   __  *                   ");
                System.out.println("  ||                   #######                    ");
                System.out.println("  ||                /#         #\\                ");
                System.out.println("  ||               /  #  0 1  #  \\               ");
                System.out.println("  ||              /  /#  1 0  #\\  \\             ");
                System.out.println("  ||             /  / #  0 1  # \\  \\            ");
                System.out.println("  ||            /  /  #  1 0  #  \\  \\           ");
                System.out.println("  ||            ***   #       #   ***             ");
                System.out.println("  ||                   #######                    ");
                System.out.println("  ||                  /   ¦   \\                  ");
                System.out.println("  ||                  |   ¦   |                   ");
                System.out.println("  ||                  |   ¦   |                   ");
                System.out.println("  ||                 _/   ¦   \\_                 ");
                System.out.println("  ||                /_____|_____\\                ");
                System.out.println("  ||                                              ");
                System.out.println("  ||                                              ");
                System.out.println("  ||                                              ");
                System.out.println("[____]                                            ");
            }
            if(ahorcado.vidas==-1){
                System.out.println("   _______________________                    ");
                System.out.println("  ||                      ¦                   ");
                System.out.println("  ||                      ¦                   ");
                System.out.println("  ||                   * * * *    MUERTO!     ");
                System.out.println("  ||                  *  X  X *               ");
                System.out.println("  ||                  *   --  *               ");
                System.out.println("  ||                   #######                ");
                System.out.println("  ||                /#         #\\            ");
                System.out.println("  ||               /  #  0 1  #  \\           ");
                System.out.println("  ||              /  /#  1 0  #\\  \\         ");
                System.out.println("  ||             /  / #  0 1  # \\  \\        ");
                System.out.println("  ||            /  /  #  1 0  #  \\  \\       ");
                System.out.println("  ||            ***   #       #   ***         ");
                System.out.println("  ||                   #######                ");
                System.out.println("  ||                  /   ¦   \\              ");
                System.out.println("  ||                  |   ¦   |               ");
                System.out.println("  ||                  |   ¦   |               ");
                System.out.println("  ||                 _/   ¦   \\_             ");
                System.out.println("  ||                /_____|_____\\            ");
                System.out.println("  ||                                          ");
                System.out.println("  ||                                          ");
                System.out.println("  ||                                          ");
                System.out.println("[____]                                        ");
                System.exit(0);
            }
        }
    } 
}