------------------------------------------------------------------------
This is the project README file. Here, you should describe your project.
Tell the reader (someone who does not know anything about this project)
all he/she needs to know. The comments should usually include at least:
------------------------------------------------------------------------

PROJECT TITLE: Hangman
PURPOSE OF PROJECT: Make a Hangman game
VERSION or DATE: 11/02/2019
AUTHORS: Juan Diego Arroyavez & �lvaro Serrano Lozano
USER INSTRUCTIONS: You write a word and they have to insert letters to win.
