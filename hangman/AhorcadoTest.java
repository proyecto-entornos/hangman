

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class AhorcadoTest.
 *
 * @author Álvaro Serrano Lozano & Juan Diego Fernandez Arroyavez
 * @version 11/02/2019
 */
public class AhorcadoTest
{
    /**
     * Default constructor for test class AhorcadoTest
     */
    Ahorcado test;
    
     public AhorcadoTest()
    {
    }

     /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
        test = new Ahorcado();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void Prueba_1(){
        test.EligePalabra();
        String palabras[]={"hola","chao","java","corazon","automovil"};
        assertEquals("adios", palabras[0]);
    }
    
    @Test
    public void Prueba_2(){
        test.EligePalabra();
        String palabras[]={"hola","chao","java","corazon","automovil"};
        assertEquals("java", palabras[2]);
    }
        
}

